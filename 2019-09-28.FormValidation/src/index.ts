import express, { Request, Response } from 'express';
import { validationResult } from 'express-validator';

import { validatePost, validateGet } from './validators';

const app = express();

// stores data from forms
const data: Request['body'] = [];

app.use(express.urlencoded({ extended: true })); // enable access form data from body

app.post('/dogs', validatePost, (req: Request, res: Response) => {
  // get validation errors
  const valErrors = validationResult(req);
  if (valErrors.isEmpty()) {
    data.push(req.body);
    // 201 = "CREATED"
    res.status(201).send(req.body);
  } else {
    // 422 = Unprocessable Entity
    res.status(422).send(valErrors.array());
  }
});

app.get('/dogs', validateGet, (req: Request, res: Response) => {
  const valErrors = validationResult(req);
  if (valErrors.isEmpty()) {
    const type = req.query.type;
    const filter = (b: typeof req.body): boolean => b.type === type;
    const filtered = data.filter(filter);

    if (filtered.length > 0) {
      res.status(200).send(filtered);
    } else {
      res.sendStatus(404);
    }
  } else {
    // 422 = Unprocessable Entity
    res.status(422).send(valErrors.array());
  }
});

app.use(express.static(__dirname + '/public'));

const server = app.listen(process.env.NODE_PORT || 8080);
server.on('listening', () => {
  console.log(server.address());
});
