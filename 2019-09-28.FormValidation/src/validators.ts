// validators.ts
// contains validator chains that check values of submitted input
// creates errors and sanitizes input (trim WS, escape HTML, etc.)

import { check } from 'express-validator';

const toLowerCase = (value: string): string => value.toLowerCase();

export const validateGet = [
  check('type', 'Please enter a type of dog.')
    .trim()
    .not()
    .isEmpty()
    .customSanitizer(toLowerCase)
    .escape(),
];

export const validatePost = validateGet.concat([
  check('name', 'Please enter a name.')
    .trim()
    .not()
    .isEmpty()
    .escape(),
  check('url', 'Please enter a valid http, https, or ftp URL.')
    .trim()
    // eslint-disable-next-line @typescript-eslint/camelcase
    .isURL({ require_protocol: true }),
]);
