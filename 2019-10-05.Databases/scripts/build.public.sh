#!/bin/sh

status='0'

find './src' -type d -name 'public' \
| while IFS= read -r src ; do
	dst="$(printf '%s' "$src" | sed 's|^\./src/|./build/|')" \
		&& dst="${dst%/*}" \
		&& mkdir -p "$dst" \
		&& cp -Rpf "$src" "$dst" \
		|| status='1'
done

exit $status
