import mongoose from 'mongoose';

const userName = process.env.mongoDBuser;
const password = process.env.mongoDBpass; // encodeURIComponent
const dbName = process.env.mongoDBname;
const cluster = process.env.mongoDBcluster;

if (!userName || !password || !dbName || !cluster) {
  throw new Error('Missing .env value');
}

const mongoDB = `mongodb+srv://${userName}:${password}@${cluster}.mongodb.net/${dbName}?retryWrites=true&w=majority`;

mongoose.connect(mongoDB, { useNewUrlParser: true, useUnifiedTopology: true });
const db = mongoose.connection;
db.on('error', (...args): void => {
  console.log('MongoDB connection error:', ...args);
});
// console.error.bind(console, 'MongoDB connection error:'));

db.once('open', () => {
  console.log('Connected to database!');
});

export default db;
