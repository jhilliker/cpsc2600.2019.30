import express, { Application } from 'express';

import db from './database/db';
import HeroModel, { findHeroes } from './models/hero';

function fn(app: Application, resolve: (val: Application) => void): void {
  app.use(express.static(__dirname + '/public'));
  app.use(express.urlencoded({ extended: true }));

  app.post('/heroes', (req, res) => {
    new HeroModel(req.body)
      .save()
      .then(obj => res.status(200).send(obj))
      .catch(err => res.status(400).send(err.message));
  });

  app.get('/heroes', (req, res) => {
    findHeroes(req.query.name)
      .then(objs => res.status(objs.length > 0 ? 200 : 404).send(objs))
      .catch(err => res.status(400).send(err.message));
  });
  db.once('open', () => {
    resolve(app);
  });
}

export default async function(app: Application): Promise<Application> {
  return new Promise((resolve /*, reject*/): void => {
    fn(app, resolve);
  });
}
