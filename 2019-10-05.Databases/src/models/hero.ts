// hero.ts

import { Document, Schema, Model, model } from 'mongoose';

export interface Hero extends Document {
  name: string;
  description: string;
  powers: string[];
}

const HeroSchema = new Schema({
  name: { type: String, maxLength: 30, required: true },
  description: String,
  powers: [{ type: String }],
});

const heroModel: Model<Hero> = model<Hero>('Hero', HeroSchema);
export default heroModel;

export async function findHeroes(nameRE?: string): Promise<Hero[]> {
  return new Promise((resolve, reject): void => {
    try {
      resolve(
        heroModel.find(nameRE ? { name: new RegExp(nameRE) } : undefined).exec()
      );
    } catch (e) {
      reject(e);
    }
  });
}
