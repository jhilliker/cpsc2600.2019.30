function fn(): void {
  const prepIngredients = (type: string): Promise<string[]> => {
    return new Promise((resolve /*, reject*/): void => {
      setTimeout(() => {
        const ingredients: string[] = [
          type,
          'flour',
          'sugar',
          'butter',
          'eggs',
        ];
        resolve(ingredients);
      }, 2000);
    });
  };

  const bakePie = (ingredients: string[]): Promise<string> => {
    return new Promise((resolve, reject): void => {
      if (ingredients && ingredients.length != 0) {
        setTimeout(() => {
          const pie = `Pie of: ${ingredients.join(',')}`;
          resolve(pie);
        }, 3000);
      } else {
        const err = new Error(`No ingredients: ${ingredients}`);
        reject(err);
      }
    });
  };

  const type = 'apple';

  console.log('Starting');
  prepIngredients(type)
    .then(ingredients => (console.log(ingredients), ingredients))
    .then(ingredients => bakePie(ingredients))
    .then(pie => (console.log(pie), pie))
    .catch(e => console.error(e));
}

// eslint-disable-next-line @typescript-eslint/no-unused-vars
function badFn(): void {
  const type = 'apple';
  let ingredients: string[] = [];

  const prepIngredients = (): void => {
    setTimeout(() => {
      ingredients = [type, 'flour', 'sugar', 'butter', 'eggs'];
      console.log('Ready:', ingredients);
    }, 2000);
  };

  const bakePie = (): void => {
    if (ingredients.length != 0) {
      console.log('Baking pie');
      setTimeout(() => {
        console.log('Done baking pie');
      }, 3000);
    } else {
      console.error('Ingredients not ready. Cannot bake.');
    }
  };

  console.log('Start');
  prepIngredients();
  bakePie();
}

fn();
