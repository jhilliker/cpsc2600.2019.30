import express from 'express';
import db from './db/connection';
import Cake from './models/Cake';

const errorMsg = 'Something went wrong!';

const app = express();

app.use(express.static(__dirname + '/public'));
app.use(express.urlencoded({ extended: true }));
app.use(express.json()); // interpret JSON in request body

app.get('/cakes', (req, res) => {
  Cake.find()
    .exec()
    .then(cakes => res.send(cakes))
    .catch(() => res.status(500).send(errorMsg));
});

app.post('/cakes', (req, res) => {
  new Cake(req.body)
    .save()
    .then(cake => {
      res.status(201).send(cake);
    })
    .catch(() => res.status(500).send(errorMsg));
});

app.delete('/cakes/:id', (req, res) => {
  Cake.deleteOne({ _id: req.params.id })
    .exec()
    .then(() => res.status(204).end())
    .catch(() => res.status(500).send(errorMsg));
});

db.once('open', () => {
  //db.then(()=>{
  const server = app.listen(process.env.NODE_PORT || 8080);
  server.on('listening', () => {
    console.log(server.address());
  });
});
db.once('error', (...args): void => {
  console.error(args);
});
