import mongoose, { Schema } from 'mongoose';

const CakeSchema = new Schema({
  name: String,
  icingFlavor: String,
});

export default mongoose.model('Cake', CakeSchema);
