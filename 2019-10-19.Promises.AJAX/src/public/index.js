//import axios from 'https://unpkg.com/axios/dist/axios.min.js';

/* global axios */

function refreshCakes() {
  // get all cakes!
  axios
    .get('/cakes')
    .then(result => {
      const out = document.getElementById('cakes-container');
      out.innerHTML = '';
      result.data.forEach(cake => {
        const li = document.createElement('li');
        li.innerHTML = `${cake.name}: ${cake.icingFlavor}`;
        li.setAttribute('id', cake._id);
        const btn = document.createElement('button');
        btn.innerHTML = 'Delete';
        btn.addEventListener('click', () => {
          deleteCake(cake._id);
        });
        li.appendChild(btn);
        out.appendChild(li);
      });
    })
    .catch(err => {
      console.error(err);
    });
}

const inName = document.getElementById('form-name');
const inIcing = document.getElementById('form-icing');

function cakeSubmit(event) {
  event.preventDefault();

  const newCake = {
    name: inName.value,
    icingFlavor: inIcing.value,
  };

  axios
    .post('/cakes', newCake)
    .then(() => {
      inName.value = '';
      inIcing.value = '';
      refreshCakes();
    })
    .catch(err => console.error(err));
}
const form = document.getElementsByTagName('form')[0];
form.action = undefined;
form.method = undefined;
form.addEventListener('submit', cakeSubmit);

function deleteCake(cakeId) {
  // /cakes/:id
  axios
    .delete(`/cakes/${cakeId}`)
    .then(() => {
      refreshCakes();
    })
    .catch(err => console.log(err));
}

refreshCakes();
