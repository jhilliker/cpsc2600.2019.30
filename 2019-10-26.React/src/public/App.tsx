import React, { Component } from 'react';

import FeaturedFilm from './FeaturedFilm';
import FilmList from './FilmList';

interface Props {
  title: string
};

interface State {
  films: {name: string, genre: string}[];
};

export default class App extends Component<Props, State> {
  // Definition component wioll go here.
  constructor(props: Props){
    super(props);
    //This constructor method will set up the component when it is created

    this.state={
      films:[
        {name:"Star wars", genre:"action"},
        {name:"Heriditary", genre:"horror"},
        {name:"Bohemian Rhapsody", genre:"drama"}
      ]
    }
    //state kepps track of data used by the component
  }
  
  //This render method will be used to output html
  render() {
    //returning JSX representation of our contents
    //Inline style must be sepcified using js objects
    //CSS gets converted to camel casing i.e background-color becomes backgroundColor
    let headingStyle = {
      color:"red",
      backgroundColor:"papayawhip"
    };
    //embed js variables into JSX code using {}
    return <>
      <h1 style={headingStyle}>{this.props.title}</h1>
      <p>I am a react program</p>
      <FeaturedFilm films={this.state.films} />
      <FilmList films={this.state.films} />
      </>; 
  }
}
