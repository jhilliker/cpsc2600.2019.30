import React, {Component } from 'react';

interface Props {
  films: {name: string, genre: string}[];
};

export default class FeaturedFilm extends Component<Props> {
  render(){
    return<>
      <h2>Featured Film!!</h2>
      <div>{this.props.films[Math.floor(Math.random()* this.props.films.length)].name}</div>
      </>;
  }
}
