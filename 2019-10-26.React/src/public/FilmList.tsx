import React, {Component } from 'react';

interface Props {
  films: {name: string, genre: string}[];
};

export default class FilmList extends Component<Props> {
  render(){
    return <>
      <h2>List of all films!!</h2>
      <ul>
        {this.props.films.map((movie, index) => {
          return <li key={index}> {movie.name}</li>;
        })}
      </ul>
      </>;
  }
}
