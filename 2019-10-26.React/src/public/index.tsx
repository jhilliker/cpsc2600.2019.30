import React from 'react';
import ReactDom from 'react-dom';

import App from './App';

ReactDom.render(<App title="title" />, document.getElementById('react-container'));
