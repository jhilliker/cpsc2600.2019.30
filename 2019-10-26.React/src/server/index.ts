// deps:
// npm install -D @types/babel-core @types/webpack @types/react @types/react-dom @babel/core @babel/preset-env @babel/preset-react babel-loader css-loader webpack webpack-cli ts-loader react react-dom

import express from 'express';

const app = express();

//app.use(express.static(__dirname + '/public'));
app.use(express.static('public'));

const server = app.listen(process.env.NODE_PORT || 8080);
server.on('listening', () => {
  console.log(server.address());
});
