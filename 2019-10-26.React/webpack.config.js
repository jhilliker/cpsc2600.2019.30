const path = require('path');
module.exports = {
  mode:"development",
  devtool:"source-map",
  //devtool: 'inline-source-map',
  entry: './src/public/index.tsx',
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, 'public'),
  },
  module: {
    rules: [
      {
        test: /\.ts|\.tsx$/,
        use: 'ts-loader',
        //include: __dirname,
        exclude: /node_modules/,
      },
      {
        test: /\.css$/,
        use:['style-loader', 'css-loader']
      },
      {
        test: /\.m?js$/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env', '@babel/preset-react' ],
            compact: false
          }
        }
      }
    ]
  }
  , resolve: {
    extensions: [ '.tsx', '.ts', '.js' ],
  }
};
