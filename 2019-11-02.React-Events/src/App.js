import React from 'react';

import NumberChooser from './NumberChooser';
import NumberDisplay from './NumberDisplay';

export default class App extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			chosenNumber: 0
		};
		
		this.choose = this.choose.bind(this);
	}
	
	choose(event, num) {
		this.setState({
			chosenNumber: num
		});
	}
	
	render() {
		return <>
			<h1>Number Chooser App</h1>
			<NumberChooser choose={this.choose}/>
			<NumberDisplay chosenNumber = { this.state.chosenNumber } />
		</>;
	}
}
