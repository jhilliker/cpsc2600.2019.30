import React from 'react';

export default class NumberChooser extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			numbers: [1, 2, 3, 4, 5]
		};
	}

	render() {
		return <><ul> {
			this.state.numbers.map(num => <li key={num}><button onClick={() => this.props.choose(event, num)}>{num}</button></li>)
		} </ul>	</>;
	}
}
