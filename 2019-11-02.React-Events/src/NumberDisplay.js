import React from 'react';
import axios from 'axios';

export default class NumberDisplay extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			fact: ''
		};
	}

	componentDidMount() {
		// called when component created & added to DOM
		axios.get(`http://numbersapi.com/${ this.props.chosenNumber }/trivia`)
		.then(result => {
			this.setState({ fact: result.data });
		})
		.catch(e => console.error(e));
	}

	componentDidUpdate(prevProps, prevState) {
		// called when props or state is updated
		
		if (prevProps.chosenNumber != this.props.chosenNumber) {
			this.componentDidMount(); // safe for now
		}
	}

	render() {
		return <>
			<h2>You chose: '{ this.props.chosenNumber }'</h2>
			<p>{ this.state.fact }</p>
		</>;
	}
}
